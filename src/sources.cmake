#This file is used to build all the targets and link them
#It provides a single-access if one adds/removes files to the project

#---------------------------------
start_folder(cppfulltext)
  #---------------------------------
  start_lib(fulltext)
    add(indexreader.cpp)
    add(indexwriter.cpp)
    add(document.cpp)
    add(query.cpp)
    add(indexserializer.cpp)
    add(filestorage.cpp)
    add(memorystorage.cpp)
    link(filesystem)
  end_lib(fulltext)
  #---------------------------------
end_folder(cppfulltext)
#---------------------------------
start_folder(test)
  #---------------------------------
  start_test(fulltext_test)
      add(reader_test.cpp)
      add(writer_test.cpp)
      add(test_main.cpp)
      add(filestorage_test.cpp)
      add(document_test.cpp)
      add(memorystorage_test.cpp)
      add(utils_test.cpp)
      link(fulltext)
      link(catch2)
  end_test(fulltext_test)
  #---------------------------------
end_folder(test)
#---------------------------------

finalize()