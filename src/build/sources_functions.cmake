#File containing the functions used by sources.txt
#Uses global properties to remember state
include(GenerateExportHeader)

set_property(GLOBAL PROPERTY scope_count "0")    #Count, so start/end functions match
set_property(GLOBAL PROPERTY folder_path "${PROJECT_SOURCE_DIR}/src/")  #The current folder structure
set_property(GLOBAL PROPERTY source_files "")     #The current list of source files, added via add 
set_property(GLOBAL PROPERTY link_files "")       #The current list of link files, added via link
set_property(GLOBAL PROPERTY target_binary "")    #The target of the current block (lib,exe,testexe,testlib)
set_property(GLOBAL PROPERTY test_libs "")        #A lists of test libs, which will get linked against test_exe in finalize
set_property(GLOBAL PROPERTY test_binary "")      #Set to the first call of start_testexe. All tests registered via start_test will get added      
set_property(GLOBAL PROPERTY install_binaries "") #a list of all the binaries that will be installed. Contains duplicates

MACRO(SUBDIRLIST result curdir)
  FILE(GLOB children RELATIVE ${curdir} ${curdir}/*)
  SET(dirlist "")
  FOREACH (child ${children})
    IF (IS_DIRECTORY ${curdir}/${child})
      LIST(APPEND dirlist ${curdir}/${child})
    ENDIF ()
  ENDFOREACH ()
  SET(${result} ${dirlist})
ENDMACRO()

#Increases global scope_count property by 1
#Should get called for each function with start_XXX
function(_increase_scope)
  get_property(old_count GLOBAL PROPERTY scope_count)
  math(EXPR new_count "${old_count}+1")
  set_property(GLOBAL PROPERTY scope_count "${new_count}")
endfunction(_increase_scope)

#Decreases global scope_count property by 1
#Should get called for each function with end_XXX
function(_decrease_scope)
  get_property(old_count GLOBAL PROPERTY scope_count)
  math(EXPR new_count "${old_count}-1")
  set_property(GLOBAL PROPERTY scope_count "${new_count}")

  if (new_count LESS 0)
    message(FATAL_ERROR "Invalid scope (called an end_XXX function to often?)")
  endif ()
endfunction(_decrease_scope)

#Makes sure that the scope counter is 0
function(_check_scope)
  get_property(new_count GLOBAL PROPERTY scope_count)
  if (NOT new_count EQUAL 0)
    message(FATAL_ERROR "Invalid scope! Call-count to end_XXX/start_XXX differ! (Count: ${new_count})")
  endif ()
endfunction(_check_scope)

#Ensure that currently target_binary is not set!
function(_ensure_no_target_binary)
  get_property(current GLOBAL PROPERTY target_binary)
  if (current)
    message(FATAL_ERROR "Call to an target-start_XXX function, without closing prev (via end_XXX). Current target: ${current}")
  endif ()
endfunction(_ensure_no_target_binary)

#Ensure that currently target_binary is set!
function(_ensure_target_binary)
  get_property(current GLOBAL PROPERTY target_binary)
  if (NOT current)
    message(FATAL_ERROR "Call to an target function, without starting block (via start_XXX)! ${current}")
  endif ()
endfunction(_ensure_target_binary)

#Resets source_files,link_files,target_binary
function(_reset_block_properties)
  set_property(GLOBAL PROPERTY source_files "")
  set_property(GLOBAL PROPERTY link_files "")
  set_property(GLOBAL PROPERTY target_binary "")
endfunction(_reset_block_properties)

function(_perform_link targetname)
  set(target_link_files)
  get_property(cur_link_files GLOBAL PROPERTY link_files)

  if (NOT cur_link_files)
    return()
  endif ()

  message(STATUS "Linking ${targetname}")

  foreach (linkfile ${cur_link_files})
      set(target_link_files ${target_link_files} ${linkfile})
      message(STATUS "  Link ${linkfile}")
  endforeach ()

  get_property(cur_global_link GLOBAL PROPERTY global_link)
  target_link_libraries(${targetname} ${target_link_files} ${cur_global_link})
  #message(STATUS "Linking ${targetname} :: ${target_link_files}")
endfunction(_perform_link)

#Starts a new subfolder (appends it to the current folder path)
#end_folder must be called if the operations for this folder are complete
function(start_folder foldername)
  _increase_scope()

  #Append it to the folder_path
  get_property(cur_folder_path GLOBAL PROPERTY folder_path)
  string(APPEND cur_folder_path "/${foldername}")
  set_property(GLOBAL PROPERTY folder_path "${cur_folder_path}")
endfunction(start_folder)

#Ends a subfolder
function(end_folder foldername)
  _decrease_scope()

  get_property(cur_folder_path GLOBAL PROPERTY folder_path)

  get_filename_component(expected_foldername ${cur_folder_path} NAME)
  if (NOT "${expected_foldername}" STREQUAL "${foldername}")
    message(FATAL_ERROR "Unexpected foldername in end_folder! Expected: '${expected_foldername}' - Actual: '${foldername}'")
  endif ()

  get_filename_component(new_folder_path ${cur_folder_path} DIRECTORY)
  set_property(GLOBAL PROPERTY folder_path "${new_folder_path}")
endfunction(end_folder)

#Adds a new file to the current block
#The folder_path will be appended to the begining of the filename
function(add filename)
  _ensure_target_binary()

  get_property(cur_folder_path GLOBAL PROPERTY folder_path)
  set(full_filename "${cur_folder_path}/${filename}")

  if (NOT EXISTS ${full_filename})
    message(FATAL_ERROR "File does not exist! ${full_filename}")
  endif ()

  get_property(cur_source_files GLOBAL PROPERTY source_files)
  set_property(GLOBAL PROPERTY source_files ${cur_source_files} ${full_filename})
endfunction(add)

#Adds a new link target to the current block
function(link linkname)
  _ensure_target_binary()

  get_property(cur_link_files GLOBAL PROPERTY link_files)
  set_property(GLOBAL PROPERTY link_files ${cur_link_files} "${linkname}")
endfunction(link)

#Starts a new lib block
function(start_lib libname)
  _ensure_no_target_binary()
  _increase_scope()

  set_property(GLOBAL PROPERTY target_binary "${libname}")
endfunction(start_lib)

#Ends a lib block. Creates the target with actual cmake commands
function(end_lib libname)
  _ensure_target_binary()
  _decrease_scope()

  get_property(cur_source_files GLOBAL PROPERTY source_files)
  get_property(cur_link_files GLOBAL PROPERTY link_files)

  add_library(${libname} SHARED ${cur_source_files})

  generate_export_header(${libname} EXPORT_FILE_NAME ${CMAKE_BINARY_DIR}/generated/gen/export_${libname}.h)
  if (WIN32)
    install(TARGETS ${libname} RUNTIME DESTINATION bin)
  elseif (UNIX)
    install(TARGETS ${libname} LIBRARY DESTINATION bin)
  endif ()
  _perform_link(${libname})

  _reset_block_properties()
endfunction(end_lib)

#Starts a new exe block
function(start_exe exename)
  _ensure_no_target_binary()
  _increase_scope()

  set_property(GLOBAL PROPERTY target_binary "${exename}")
endfunction(start_exe)

#Ends an exe block. Creates the target with actual cmake commands
function(end_exe exename)
  _ensure_target_binary()
  _decrease_scope()

  get_property(cur_source_files GLOBAL PROPERTY source_files)

  add_executable(${exename} ${cur_source_files})
  _perform_link(${exename})

  install(TARGETS ${exename}
    RUNTIME DESTINATION bin)

  _reset_block_properties()
endfunction(end_exe)

#Starts a new test block
function(start_test testname)
  _ensure_no_target_binary()
  _increase_scope()

  set_property(GLOBAL PROPERTY target_binary "${testname}")
endfunction(start_test)

function(end_test testname)
  end_exe("${testname}")
  #target_compile_definitions(${testname} PRIVATE "-DCATCH_CONFIG_FAST_COMPILE")
  add_test(NAME ${testname} COMMAND ${testname} -o report.xml -r junit)
endfunction(end_test)

function(create_symlinks)
  foreach (path_file ${ARGN})
    get_filename_component(folder ${path_file} NAME)

    # Get OS dependent path to use in `execute_process`
    file(TO_NATIVE_PATH "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/${folder}" link)
    #file(TO_NATIVE_PATH "${CMAKE_CURRENT_BINARY_DIR}/${folder}" link)
    file(TO_NATIVE_PATH "${path_file}" target)

    if (EXISTS ${link}) #Already existing symlink, skip
      continue()
    endif ()

    if (UNIX)
      set(command ln -s ${target} ${link})
    else ()
      #On windows, force correct slashes (mingw32 makefile target gets it wrong), otherwise mklink fails
      STRING(REGEX REPLACE "/" "\\\\" link ${link})
      STRING(REGEX REPLACE "/" "\\\\" target ${target})
      if (IS_DIRECTORY ${path_file})
        file(REMOVE_RECURSE ${link})
        set(command cmd.exe /c mklink /D ${link} ${target})
      else ()
        file(REMOVE ${link})
        message("cmd.exe /c mklink ${link} ${target}")
        set(command cmd.exe /c mklink ${link} ${target})
      endif ()
    endif ()

    execute_process(COMMAND ${command}
      RESULT_VARIABLE result
      ERROR_VARIABLE output)

    if (NOT ${result} EQUAL 0)
      message(FATAL_ERROR "Could not create symbolic link for: ${target} --> ${output}")
    endif ()

  endforeach (path_file)
endfunction(create_symlinks)

#Gets called once at the end
function(finalize)
  _check_scope()

endfunction(finalize)

