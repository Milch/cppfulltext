set(ext_dir ${PROJECT_SOURCE_DIR}/ext/)

#Setup libs
add_library(catch2 INTERFACE IMPORTED)
set_property(TARGET catch2 PROPERTY INTERFACE_INCLUDE_DIRECTORIES ${ext_dir}/catch2/)

add_library(filesystem INTERFACE IMPORTED)
if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
  set_property(TARGET filesystem PROPERTY INTERFACE_LINK_LIBRARIES "-lstdc++fs")
endif()