#Used for exporting symbols from shared libs
set(CMAKE_CXX_VISIBILITY_PRESET hidden)
set(CMAKE_VISIBILITY_INLINES_HIDDEN 1)

#Avoid relinking all the libraries if headers did not change
set(CMAKE_LINK_DEPENDS_NO_SHARED 1)

#Use explicit compilercommands because dockerimage used for building has cmake 3.7 and it does not 
#know c++17
if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
  # using GCC
  SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} --std=c++17" )
elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "MSVC")
  # using Visual Studio C++
  SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /std:c++17 /EHsc /D_CRT_SECURE_NO_WARNINGS" )
endif()
