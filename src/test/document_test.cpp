#include <catch2/catch.hpp>

#include "cppfulltext/document.h"

namespace CppFulltext
{

TEST_CASE("Document.add test", "[DocumentTest]")
{
  Document document;

  document.add("name", "testname");
  document.add("content", "long content of the document");

  REQUIRE(document.get("content") == "long content of the document");
  REQUIRE(document.get("name") == "testname");
}

TEST_CASE("Document.remove test", "[DocumentTest]")
{
	Document document;
	document.add("content", "long content of the document");

	REQUIRE(document.get("content") == "long content of the document");
	document.remove("content");

	REQUIRE(document.get("content").empty());
}

TEST_CASE("Document.getFields test", "[DocumentTest]")
{
	Document document;

	document.add("name", "testname");
	document.add("content", "long content of the document");

	auto fields = document.getFields();
	
	REQUIRE(fields["content"] == "long content of the document");
	REQUIRE(fields["name"] == "testname");
}
}