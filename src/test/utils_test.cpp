#include <catch2/catch.hpp>

#include "cppfulltext/memorystorage.h"
#include "cppfulltext/utils.h"

namespace
{
}

namespace CppFulltext
{

TEST_CASE("Save Utils load/save int test", "[UtilsTest]")
{
  auto backend(std::make_shared<MemoryStorageBackend>());
  auto storage = createMemoryStorage(backend);

  {
    char test[sizeof(unsigned int)];
    int const primitive = 99;

    REQUIRE(write(test, primitive) == sizeof(int));
    REQUIRE(bytesToWrite(primitive) == sizeof(int));

    storage->write("test", test, sizeof(int));
    std::unique_ptr<std::istream> stream = storage->read("test");
    int const returnedPrimitive(read<int>(*stream));
    REQUIRE(returnedPrimitive == 99);
  }
}

TEST_CASE("Save Utils load/save std::size_t test", "[UtilsTest]")
{
  auto backend(std::make_shared<MemoryStorageBackend>());
  auto storage = createMemoryStorage(backend);

  {
    char test[sizeof(std::size_t)];
    std::size_t const primitive = 99;
    REQUIRE(write(test, primitive) == sizeof(std::size_t));
    REQUIRE(bytesToWrite(primitive) == sizeof(std::size_t));

    storage->write("test", test, sizeof(std::size_t));
    std::unique_ptr<std::istream> stream = storage->read("test");

    std::size_t const returnedPrimitive(read<std::size_t>(*stream));
    REQUIRE(returnedPrimitive == 99);
  }
}

TEST_CASE("Save Utils load/save char test", "[UtilsTest]")
{
  auto backend(std::make_shared<MemoryStorageBackend>());
  auto storage = createMemoryStorage(backend);

  {
    char test[sizeof(unsigned char)];
    char const primitive = 99;
    REQUIRE(write(test, primitive) == sizeof(char));
    REQUIRE(bytesToWrite(primitive) == sizeof(char));

    storage->write("test", test, sizeof(char));
    std::unique_ptr<std::istream> stream = storage->read("test");

    char const returnedPrimitive(read<char>(*stream));
    REQUIRE(returnedPrimitive == 99);
  }
}

TEST_CASE("Save Utils load/save string", "[UtilsTest]")
{
  auto backend(std::make_shared<MemoryStorageBackend>());
  auto storage = createMemoryStorage(backend);

  {
    char test[sizeof(std::size_t) + 5]; // lenght count + actual string lenght
    char const* string = "12345";

    REQUIRE(write(test, string, 5) == sizeof(std::size_t) + 5);
    REQUIRE(bytesToWrite(string, 5) == sizeof(std::size_t) + 5);

    storage->write("test", test, sizeof(std::size_t) + 5);
    std::unique_ptr<std::istream> stream = storage->read("test");

    std::string const result(read<std::string>(*stream));
    REQUIRE(result == "12345");
  }
}

TEST_CASE("Save Utils load/save empty string", "[UtilsTest]")
{
  auto backend(std::make_shared<MemoryStorageBackend>());
  auto storage = createMemoryStorage(backend);

  {
    char test[sizeof(std::size_t) + 0]; // lenght count + actual string lenght
    char const* string = "";

    REQUIRE(write(test, string, 0) == sizeof(std::size_t) + 0);
    REQUIRE(bytesToWrite(string, 0) == sizeof(std::size_t) + 0);

    // Fix this, does not work with gcc
    /*storage->write("test", test, 0);
    std::unique_ptr<std::istream> stream = storage->read("test");

    std::string const result(read<std::string>(*stream));
    REQUIRE(result.empty() == true);*/
  }
}
}