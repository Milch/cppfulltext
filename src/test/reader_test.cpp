#include <catch2/catch.hpp>

#include "cppfulltext/indexreader.h"
#include "cppfulltext/memorystorage.h"

namespace
{
}

namespace CppFulltext
{

TEST_CASE("Basic test", "[IndexReaderTest]")
{
  auto backend(std::make_shared<MemoryStorageBackend>());
  IndexReader reader(createMemoryStorage(backend));
  REQUIRE(reader.isValid() == false);
}

TEST_CASE("Read non existing document", "[IndexReaderTest]")
{
  auto backend(std::make_shared<MemoryStorageBackend>());
  IndexReader reader(createMemoryStorage(backend));
  REQUIRE(reader.isValid() == false);

  REQUIRE(reader.get(9000) == nullptr);
}

// TODO: test IndexReader::update()
}