#include <catch2/catch.hpp>

#include "cppfulltext/document.h"
#include "cppfulltext/filestorage.h"
#include "cppfulltext/indexreader.h"
#include "cppfulltext/indexwriter.h"
#include "cppfulltext/memorystorage.h"
#include "cppfulltext/query.h"

namespace CppFulltext
{

TEST_CASE("Basic Writer test with memorystorage", "[IndexWriterTest]")
{
  auto backend(std::make_shared<MemoryStorageBackend>());
  IndexWriter writer(createMemoryStorage(backend));
  REQUIRE(writer.isValid() == true);

  {
    Document document;
    document.add("name", "a");
    document.add("content", "ac");
    writer.addDocuments(&document, 1);
  }

  {
    Document document;
    document.add("name", "b");
    document.add("content", "bc");
    writer.addDocuments(&document, 1);
  }

  {
    Document documents[2];
    documents[0].add("name", "c");
    documents[0].add("content", "cc");

    documents[1].add("name", "d");
    documents[1].add("content", "dc");
    writer.addDocuments(documents, 2);
  }

  writer.flush();

  {
    IndexReader reader(createMemoryStorage(backend));
    auto query = reader.query();
    REQUIRE(query.get() != nullptr);
    REQUIRE(query->contains("name", "c") == query.get());
    auto result = query->perform();
    REQUIRE(result.size() == 1);

    // Get document
    auto doc = reader.get(result[0]);
    REQUIRE(doc.get() != nullptr);

    REQUIRE(doc->get("name") == "c");
    REQUIRE(doc->get("content") == "cc");
  }
}

TEST_CASE("Basic Writer test with filestorage", "[IndexWriterTest]")
{
  IndexWriter writer(createFileStorage("test123", true));
  REQUIRE(writer.isValid() == true);

  {
    Document document;
    document.add("name", "a");
    document.add("content", "ac");
    writer.addDocuments(&document, 1);
  }

  {
    Document document;
    document.add("name", "b");
    document.add("content", "bc");
    writer.addDocuments(&document, 1);
  }

  {
    Document documents[2];
    documents[0].add("name", "c");
    documents[0].add("content", "cc");

    documents[1].add("name", "d");
    documents[1].add("content", "dc");
    writer.addDocuments(documents, 2);
  }

  writer.flush();

  {
    IndexReader reader(createFileStorage("test123"));
    auto query = reader.query();
    REQUIRE(query.get() != nullptr);
    REQUIRE(query->contains("name", "c") == query.get());
    auto result = query->perform();
    REQUIRE(result.size() == 1);

    // Get document
    auto doc = reader.get(result[0]);
    REQUIRE(doc.get() != nullptr);

    REQUIRE(doc->get("name") == "c");
    REQUIRE(doc->get("content") == "cc");
  }
}

TEST_CASE("Query contains chain test", "[FullTextTest]")
{
  auto backend(std::make_shared<MemoryStorageBackend>());
  IndexWriter writer(createMemoryStorage(backend));
  REQUIRE(writer.isValid() == true);

  {
    Document document;
    document.add("name", "a");
    document.add("content", "ac");
    writer.addDocuments(&document, 1);
  }

  {
    Document document;
    document.add("name", "b");
    document.add("content", "cc");
    writer.addDocuments(&document, 1);
  }

  {
    Document documents[2];
    documents[0].add("name", "c");
    documents[0].add("content", "cc");

    documents[1].add("name", "c");
    documents[1].add("content", "dc");
    writer.addDocuments(documents, 2);
  }

  writer.flush();

  {
    IndexReader reader(createMemoryStorage(backend));
    auto query = reader.query();
    REQUIRE(query.get() != nullptr);
    REQUIRE(query->contains("name", "c")->contains("content", "cc") == query.get());
    auto result = query->perform();
    REQUIRE(result.size() == 1);

    // Get document
    auto doc = reader.get(result[0]);
    REQUIRE(doc.get() != nullptr);

    REQUIRE(doc->get("name") == "c");
    REQUIRE(doc->get("content") == "cc");
  }
}

TEST_CASE("IndexWriter deleteNonExisting", "[IndexWriterTest]")
{
  auto backend(std::make_shared<MemoryStorageBackend>());
  IndexWriter writer(createMemoryStorage(backend));
  REQUIRE(writer.isValid() == true);

  REQUIRE(writer.deleteDocument(9000) == false);
}

TEST_CASE("IndexWriter delete", "[IndexWriterTest]")
{
  auto backend(std::make_shared<MemoryStorageBackend>());
  IndexWriter writer(createMemoryStorage(backend));
  REQUIRE(writer.isValid() == true);

  {
    Document documents[1];
    documents[0].add("name", "c");
    documents[0].add("content", "cc");
    writer.addDocuments(documents, 1);
  }

  writer.flush();

  {
    IndexReader reader(createMemoryStorage(backend));
    auto query = reader.query();
    REQUIRE(query.get() != nullptr);
    REQUIRE(query->contains("name", "c") == query.get());
    auto result = query->perform();
    REQUIRE(result.size() == 1);

    // delete document
    REQUIRE(writer.deleteDocument(result[0]) == true);
    REQUIRE(writer.deleteDocument(result[0]) == false); // not found
  }
}

TEST_CASE("IndexWriter InvalidAfterClosing", "[IndexWriterTest]")
{
  auto backend(std::make_shared<MemoryStorageBackend>());
  IndexWriter writer(createMemoryStorage(backend));
  REQUIRE(writer.isValid() == true);

  writer.close();

  REQUIRE(writer.isValid() == false);
}

TEST_CASE("Deletion Writer test", "[WriterTest]")
{
  auto backend(std::make_shared<MemoryStorageBackend>());
  IndexWriter writer(createMemoryStorage(backend));
  REQUIRE(writer.isValid() == true);

  {
    Document document;
    document.add("name", "a");
    document.add("content", "ac");
    writer.addDocuments(&document, 1);
  }

  {
    Document document;
    document.add("name", "b");
    document.add("content", "bc");
    writer.addDocuments(&document, 1);
  }

  writer.flush();
  writer.deleteDocument(2);
  writer.flush();

  {
    IndexReader reader(createMemoryStorage(backend));
    auto query = reader.query();
    REQUIRE(query.get() != nullptr);
    REQUIRE(query->contains("name", "b") == query.get());
    auto result = query->perform();
    REQUIRE(result.size() == 0);
  }
}

TEST_CASE("Writer open existig index test", "[WriterTest]")
{
  auto backend(std::make_shared<MemoryStorageBackend>());

  {
    IndexWriter writer(createMemoryStorage(backend));
    REQUIRE(writer.isValid() == true);

    {
      Document document;
      document.add("name", "a");
      document.add("content", "ac");
      writer.addDocuments(&document, 1);
    }

    writer.flush();
  }

  {
    IndexWriter writer(createMemoryStorage(backend));
    REQUIRE(writer.isValid() == true);

    {
      Document document;
      document.add("name", "a");
      document.add("content", "bc");
      writer.addDocuments(&document, 1);
    }

    writer.flush();
  }

  {
    IndexReader reader(createMemoryStorage(backend));
    auto query = reader.query();
    REQUIRE(query.get() != nullptr);
    REQUIRE(query->contains("name", "a") == query.get());
    auto result = query->perform();
    REQUIRE(result.size() == 2);
  }
}
}