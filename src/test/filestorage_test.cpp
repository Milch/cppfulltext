#include <catch2/catch.hpp>

#include "cppfulltext/filestorage.h"
#include "cppfulltext/indexstorage.h"
#include "cppfulltext/utils.h"

#include <cstring>

namespace
{
}

namespace CppFulltext
{

TEST_CASE("FileStorage read nonexisting", "[FileStorageTest]")
{
  std::unique_ptr<IndexStorage> storage = createFileStorage("index");
  REQUIRE(storage.get() != nullptr);

  {
    auto istream = storage->read("nonexisting.txt");
    REQUIRE(istream.get() == nullptr);
  }
}

TEST_CASE("FileStorage WriteTest", "[FileStorageTest]")
{
  std::unique_ptr<IndexStorage> storage = createFileStorage("index");
  REQUIRE(storage.get() != nullptr);

  REQUIRE(storage->write("test123.txt", "123", 3) == true);

  {
    auto istream = storage->read("test123.txt");
    REQUIRE(istream.get() != nullptr);
    REQUIRE(istream->good());

    char buf[5];
    istream->read(buf, 5);
    REQUIRE(istream->gcount() == 3);
    REQUIRE(std::strncmp(buf, "123", 3) == 0);
  }
}

TEST_CASE("FileStorage getFileListTest", "[FileStorageTest]")
{
  std::unique_ptr<IndexStorage> storage = createFileStorage("index");
  REQUIRE(storage.get() != nullptr);

  REQUIRE(storage->write("test123.txt", "123", 3) == true);
  REQUIRE(storage->write("test124.txt", "124", 3) == true);

  auto fileList = storage->getFileList();
  REQUIRE(fileList.size() == 2);
  REQUIRE(fileList[0] == "test123.txt");
  REQUIRE(fileList[1] == "test124.txt");
}

TEST_CASE("FileStorage DeleteTest", "[FileStorageTest]")
{
  std::unique_ptr<IndexStorage> storage = createFileStorage("index");
  REQUIRE(storage.get() != nullptr);

  REQUIRE(storage->write("test123.txt", "123", 3) == true);
  storage->deleteFile("test123.txt");
  {
    auto istream = storage->read("test123.txt");
    REQUIRE(istream.get() == nullptr);
  }
}

TEST_CASE("FileStorage Write/Read utils test", "[FileStorageTest]")
{
  std::unique_ptr<IndexStorage> storage = createFileStorage("index");
  REQUIRE(storage.get() != nullptr);

  unsigned int const testValue = 2;
  {
    char buf[100];
    write(buf, testValue);
    REQUIRE(storage->write("test123.txt", buf, 100) == true);
  }

  {
    auto stream = storage->read("test123.txt");
    unsigned int const testValue2(read<unsigned int>(*stream));
    REQUIRE(testValue2 == testValue);
  }
}
}