#include <catch2/catch.hpp>

#include "cppfulltext/indexstorage.h"
#include "cppfulltext/memorystorage.h"

#include <cstring>

namespace CppFulltext
{

TEST_CASE("MemoryStorage read nonexisting", "[MemoryStorageTest]")
{
  std::unique_ptr<IndexStorage> storage =
    createMemoryStorage(std::make_shared<MemoryStorageBackend>());
  REQUIRE(storage.get() != nullptr);

  {
    auto istream = storage->read("nonexisting.txt");
    REQUIRE(istream.get() == nullptr);
  }
}

TEST_CASE("MemoryStorage WriteTest", "[MemoryStorageTest]")
{
  std::unique_ptr<IndexStorage> storage =
    createMemoryStorage(std::make_shared<MemoryStorageBackend>());
  REQUIRE(storage.get() != nullptr);

  REQUIRE(storage->write("test123.txt", "123", 3) == true);

  {
    auto istream = storage->read("test123.txt");
    REQUIRE(istream.get() != nullptr);
    char buf[5];
    istream->read(buf, 5);
    REQUIRE(istream->gcount() == 3);
    REQUIRE(std::strncmp(buf, "123", 3) == 0);
  }
}

TEST_CASE("MemoryStorage getFileListTest", "[MemoryStorageTest]")
{
	std::unique_ptr<IndexStorage> storage = createMemoryStorage(std::make_shared<MemoryStorageBackend>());
	REQUIRE(storage.get() != nullptr);

	REQUIRE(storage->write("test123.txt", "123", 3) == true);
	REQUIRE(storage->write("test124.txt", "124", 3) == true);

	
	auto fileList = storage->getFileList();
	REQUIRE(fileList.size() == 2);
	REQUIRE(fileList[0] == "test123.txt");
	REQUIRE(fileList[1] == "test124.txt");
}

TEST_CASE("MemoryStorage DeleteTest", "[MemoryStorageTest]")
{
  std::unique_ptr<IndexStorage> storage =
    createMemoryStorage(std::make_shared<MemoryStorageBackend>());
  REQUIRE(storage.get() != nullptr);

  REQUIRE(storage->write("test123.txt", "123", 3) == true);
  storage->deleteFile("test123.txt");
  {
    auto istream = storage->read("test123.txt");
    REQUIRE(istream.get() == nullptr);
  }
}
}