#include "document.h"

namespace CppFulltext
{
Document::Document() {}

Document::Document(Document const& other) : fields(other.fields) {}

void Document::add(std::string const& field, std::string const& content)
{
  fields.insert_or_assign(field, content);
}

void Document::remove(std::string const& field) { fields.erase(field); }

std::string Document::get(std::string const& field) const
{
  auto it = fields.find(field);
  if(it == fields.end())
    return std::string();
  else
    return it->second;
}

std::unordered_map<std::string, std::string> const& Document::getFields() const { return fields; }
std::unordered_map<std::string, std::string>& Document::getFields() { return fields; }
}