#include "memorystorage.h"
#include "indexstorage.h"

#include <fstream>
#include <map>
#include <memory>

namespace CppFulltext
{
namespace
{
  class MemoryStream : public std::istream
  {
  private:
    class ByteBuffer : public std::basic_streambuf<char>
    {
    public:
      ByteBuffer(char* buffer, std::size_t const length) { setg(buffer, buffer, buffer + length); }
    };

    ByteBuffer byteBuffer;

  public:
    MemoryStream(char* buffer, std::size_t const length)
      : std::istream(&byteBuffer), byteBuffer(buffer, length)
    {
      // assign again, because buffer is not initialized correctly during construction
      // this is because of a flaw with the std::istream api, which cannot construct an empty
      // istream also its dangerous to directly create the std::istream with a not-yet initalized
      // member.
      rdbuf(&byteBuffer);
    }
  };
}

class MemoryStorage : public IndexStorage
{
private:
  std::shared_ptr<MemoryStorageBackend> backend;

public:
  MemoryStorage(std::shared_ptr<MemoryStorageBackend> backend) : backend(backend) {}

  ~MemoryStorage() = default;

  std::vector<std::string> getFileList()
  {
    std::vector<std::string> fileNames;
    for(std::map<std::string, std::string>::iterator it = backend->fileContentMap.begin();
        it != backend->fileContentMap.end(); ++it)
    {
      fileNames.push_back(it->first);
    }
    return fileNames;
  }

  std::unique_ptr<std::istream> read(char const* fileName)
  {
    std::lock_guard<std::mutex>(backend->mutex);
    auto it = backend->fileContentMap.find(fileName);

    if(it == backend->fileContentMap.end()) return std::unique_ptr<std::istream>();

    auto ptr = std::make_unique<MemoryStream>(it->second.data(), it->second.size());

    if(!ptr->good()) return std::unique_ptr<std::istream>();

    return std::move(ptr);
  }

  bool write(char const* fileName, char const* content, std::size_t const length)
  {
    std::string stringContent(content, length);

    {
      std::lock_guard<std::mutex>(backend->mutex);
      backend->fileContentMap.insert_or_assign(fileName, std::move(stringContent));
    }

    return true;
  }

  void deleteFile(char const* fileName)
  {
    std::lock_guard<std::mutex>(backend->mutex);
    backend->fileContentMap.erase(fileName);
  }
};

std::unique_ptr<IndexStorage> createMemoryStorage(std::shared_ptr<MemoryStorageBackend> backend)
{
  return std::make_unique<MemoryStorage>(backend);
}
}