#include "indexwriter.h"
#include "document.h"
#include "indexstorage.h"
#include "utils.h"

#include <iostream>

namespace CppFulltext
{

IndexWriter::IndexWriter(std::unique_ptr<IndexStorage>&& storage)
  : indexStorage(std::move(storage)), docID(0)
{
  auto pair = Serializer::getLatestIndex(*IndexWriter::indexStorage);
  if(pair.second == 0)
  {
    isValidInternal = true;
  }
  else
  {
    isValidInternal = Serializer::deserialize(*IndexWriter::indexStorage, pair.first.c_str(),
                                              contents, documents, deletedDocuments);
  }
}

IndexWriter::~IndexWriter() {}

bool IndexWriter::isValid() const { return isValidInternal; }
void IndexWriter::addDocuments(Document* docs, std::size_t const docNum)
{
  for(std::size_t i = 0; i < docNum; ++i)
  {
    Document* const cur = docs + i;
    unsigned int const currentDocID = ++docID;

    documents.emplace(std::make_pair(currentDocID, std::make_unique<Document>(*cur)));

    // For each field
    for(auto const & [ key, value ] : cur->getFields())
    {
      // Get or create entry for fieldname in current segment
      auto fieldEntry =
        contents.insert(Serializer::ContentMap::value_type(key, Serializer::Entry())).first;

      // Tokenize the field contents (currently hardcoded to ' ' character)
      char const* str = value.c_str();
      do
      {
        char const* begin = str;

        while(*str != ' ' && *str)
          str++;

        {
          // Find token entry in current field
          std::string const token(begin, str);
          (*fieldEntry)
            .second.insert(Serializer::Entry::value_type(token, Serializer::DocumentIDContainer()))
            .first->second.push_back(currentDocID);
        }

      } while(0 != *str++);
    }
  }
}
bool IndexWriter::deleteDocument(unsigned int documentId)
{
  if(documents.find(documentId) == documents.end()) return false;
  if(deletedDocuments.find(documentId) != deletedDocuments.end()) return false;

  deletedDocuments.insert(documentId);
  return true;
}
void IndexWriter::flush()
{
  if(!Serializer::serialize(*indexStorage, Serializer::getNextIndex(*indexStorage).first.c_str(),
                            contents, documents, deletedDocuments))
  {
    std::cerr << "Failed flushing!" << std::endl;
  }
}
void IndexWriter::close()
{
  flush();
  isValidInternal = false;
}
}
