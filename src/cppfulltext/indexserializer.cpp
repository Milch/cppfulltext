#include "indexserializer.h"

#include "indexstorage.h"
#include "utils.h"

namespace CppFulltext
{

namespace Serializer
{

  bool serialize(IndexStorage& storage,
                 char const* file,
                 ContentMap const& contents,
                 std::unordered_map<unsigned int, std::unique_ptr<Document>> const& documents,
                 std::set<unsigned int> deletedDocuments)
  {
    // Write contents into internal buffer
    std::vector<char> data;

    std::size_t fieldCount(0);
    data.resize(data.size() + bytesToWrite(fieldCount)); // Reserve but written later
    std::size_t cur(bytesToWrite(fieldCount));
    for(auto const & [ key, termlist ] : contents)
    {
      ++fieldCount;

      // Write field name
      data.resize(data.size() + bytesToWrite(key.c_str(), key.length()));
      cur += write(data.data() + cur, key.c_str(), key.length());

      // Write count of terms
      data.resize(data.size() + bytesToWrite(termlist.size()));
      cur += write(data.data() + cur, termlist.size());

      // Write the list of terms
      for(auto const & [ term, doclist ] : termlist)
      {
        data.resize(data.size() + bytesToWrite(term.c_str(), term.length()));
        cur += write(data.data() + cur, term.c_str(), term.length());

        // Write the count of documents following this
        data.resize(data.size() + bytesToWrite(doclist.size()));
        cur += write(data.data() + cur, doclist.size());

        // Write the document numbers containing the term
        for(std::size_t i = 0; i < doclist.size(); ++i)
        {
          data.resize(data.size() + bytesToWrite(doclist[i]));
          cur += write(data.data() + cur, doclist[i]);
        }
      }
    }

    // Write deleted documents - shift this to own file!
    data.resize(data.size() + bytesToWrite(deletedDocuments.size()));
    cur += write(data.data() + cur, deletedDocuments.size());

    for(auto const& docId : deletedDocuments)
    {
      data.resize(data.size() + bytesToWrite(docId));
      cur += write(data.data() + cur, docId);
    }

    // Write documents
    write(data.data(), fieldCount); // Write in beginning number of fields

    // Write document data - doc count
    data.resize(data.size() + bytesToWrite(documents.size()));
    cur += write(data.data() + cur, documents.size());

    // Write the data for each document
    for(auto const & [ docId, document ] : documents)
    {
      // Write documentId
      data.resize(data.size() + bytesToWrite(docId));
      cur += write(data.data() + cur, docId);

      // Reserve number of fields
      std::size_t docFieldCount(0);
      std::size_t docFieldCountOffset(cur);
      data.resize(data.size() + bytesToWrite(docFieldCount)); // Reserve but written later
      cur += bytesToWrite(fieldCount);

      for(auto const & [ field, content ] : document->getFields())
      {
        ++docFieldCount;
        // Write field name
        data.resize(data.size() + bytesToWrite(field.c_str(), field.length()));
        cur += write(data.data() + cur, field.c_str(), field.length());

        // Write field content
        data.resize(data.size() + bytesToWrite(content.c_str(), content.length()));
        cur += write(data.data() + cur, content.c_str(), content.length());
      }

      write(data.data() + docFieldCountOffset,
            docFieldCount); // Write in beginning number of fields
    }

    // Flush finished changes
    return storage.write(file, data.data(), data.size());
  }

  bool deserialize(IndexStorage& storage,
                   char const* file,
                   ContentMap& contents,
                   std::unordered_map<unsigned int, std::unique_ptr<Document>>& documents,
                   std::set<unsigned int>& deletedDocuments)
  {
    auto istream = storage.read(file);
    if(istream.get() == nullptr)
    {
      return true; // Index does not exist but its ok!
    }

    // Read inverted index
    {
      contents.clear();
      std::size_t const fieldCount(read<std::size_t>(*istream));
      contents.reserve(fieldCount);

      for(std::size_t i = 0; i < fieldCount; ++i)
      {
        std::string const fieldName(read<std::string>(*istream));
        std::size_t const termCount(read<std::size_t>(*istream));

        Entry entry;
        entry.reserve(termCount);

        for(std::size_t k = 0; k < termCount; ++k)
        {
          std::string const term(read<std::string>(*istream));
          std::size_t const docIdsCount(read<std::size_t>(*istream));
          DocumentIDContainer docIds;
          docIds.reserve(docIdsCount);

          for(std::size_t j = 0; j < docIdsCount; ++j)
          {
            docIds.push_back(read<unsigned int>(*istream));
          }

          entry.emplace(std::move(term), std::move(docIds));
        }

        contents.emplace(std::move(fieldName), std::move(entry));
      }
    }

    // Read deleted documents
    std::size_t const deletedDocumentsCount(read<std::size_t>(*istream));
    deletedDocuments.clear();

    for(std::size_t i = 0; i < deletedDocumentsCount; ++i)
    {
      unsigned int const docId(read<unsigned int>(*istream));
      deletedDocuments.insert(docId);
    }

    // Read documents
    std::size_t const documentCount(read<std::size_t>(*istream));
    documents.clear();
    documents.reserve(documentCount);

    for(std::size_t i = 0; i < documentCount; ++i)
    {
      unsigned int const docId(read<unsigned int>(*istream));
      std::size_t const fieldCount(read<std::size_t>(*istream));

      std::unique_ptr<Document> document(std::make_unique<Document>());
      auto& documentFields = document->getFields();
      documentFields.reserve(fieldCount);

      for(std::size_t k = 0; k < fieldCount; ++k)
      {
        std::string const fieldName(read<std::string>(*istream));
        std::string const fieldContent(read<std::string>(*istream));
        documentFields.emplace(std::move(fieldName), std::move(fieldContent));
      }

      documents.emplace(docId, std::move(document));
    }

    return true;
  }

  std::pair<std::string, std::size_t> getLatestIndex(IndexStorage& storage)
  {
    std::vector<std::string> const list = storage.getFileList();

    std::size_t highest = 0;
    std::string retVal;
    for(auto const& entry : list)
    {
      auto start = entry.find("index_");
      if(start == std::string::npos) continue;
      auto end = entry.find(".", start);
      if(end == std::string::npos) continue;

      //+6 because on index_
      std::size_t const gen = std::stoi(entry.substr(start + 6, end - (start + 6)));
      if(gen > highest)
      {
        highest = gen;
        retVal = entry;
      }
    }

    return {retVal, highest};
  }

  std::pair<std::string, std::size_t> getNextIndex(IndexStorage& storage)
  {
    std::pair<std::string, std::size_t> nextVal = getLatestIndex(storage);
    if(nextVal.first.empty())
    {
      return {std::string("index_1.index"), 1};
    }

    std::string retVal = "index_";
    retVal += std::to_string(nextVal.second + 1);
    retVal += ".index";
    return {retVal, nextVal.second + 1};
  }
}
}