#pragma once

#include "gen/export_fulltext.h"
#include <memory>
#include <set>
#include <string>
#include <unordered_map>
#include <vector>

#include "document.h"
#include "indexserializer.h"

namespace CppFulltext
{
class IndexStorage;

class IndexWriter
{
private:
  std::unique_ptr<IndexStorage> indexStorage;
  unsigned int docID;
  bool isValidInternal;

  Serializer::ContentMap contents;
  std::unordered_map<unsigned int, std::unique_ptr<Document>> documents;
  std::set<unsigned int> deletedDocuments;

public:
  FULLTEXT_EXPORT explicit IndexWriter(std::unique_ptr<IndexStorage>&& indexStorage);
  FULLTEXT_EXPORT ~IndexWriter();
  FULLTEXT_EXPORT bool isValid() const;
  FULLTEXT_EXPORT void addDocuments(Document* docs, std::size_t const docNum);
  FULLTEXT_EXPORT bool deleteDocument(unsigned int documentId);
  FULLTEXT_EXPORT void flush();
  FULLTEXT_EXPORT void close();
};
}