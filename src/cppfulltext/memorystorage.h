#pragma once

#include "cppfulltext/indexstorage.h"

#include "gen/export_fulltext.h"
#include <map>
#include <memory>
#include <mutex>

namespace CppFulltext
{
class IndexStorage;

/**
 * This class is needed in order to share a single memory storage with multiple readers/single write
 * in the same application.
 */
struct MemoryStorageBackend
{
  std::map<std::string, std::string> fileContentMap;
  std::mutex mutex;
};

FULLTEXT_EXPORT std::unique_ptr<IndexStorage> createMemoryStorage(
  std::shared_ptr<MemoryStorageBackend> backend);
}