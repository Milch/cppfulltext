#pragma once

#include "document.h"

#include <memory>
#include <set>
#include <unordered_map>
#include <vector>

namespace CppFulltext
{
class IndexStorage;

namespace Serializer
{
  // Contains the documentIDs which have a term
  using DocumentIDContainer = std::vector<unsigned int>;

  // A term -> documents map
  using Entry = std::unordered_map<std::string, DocumentIDContainer>;

  // A fieldName -> Entry map
  using ContentMap = std::unordered_map<std::string, Entry>;

  bool serialize(IndexStorage& storage,
                 char const* file,
                 ContentMap const& contents,
                 std::unordered_map<unsigned int, std::unique_ptr<Document>> const& documents,
                 std::set<unsigned int> deletedDocuments);

  bool deserialize(IndexStorage& storage,
                   char const* file,
                   ContentMap& contents,
                   std::unordered_map<unsigned int, std::unique_ptr<Document>>& documents,
                   std::set<unsigned int>& deletedDocuments);

  std::pair<std::string, std::size_t> getLatestIndex(IndexStorage& storage);
  std::pair<std::string, std::size_t> getNextIndex(IndexStorage& storage);
}
}