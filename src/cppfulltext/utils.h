#pragma once

#include <cstring>
#include <fstream>
#include <iostream>
#include <string>
#include <string_view>
#include <type_traits>
#include <unordered_map>
#include <utility>

namespace CppFulltext
{
namespace
{

  // Writes the given type into the data bytewise
  // Returns the number of bytes written
  template <typename T> std::size_t write(char* data, T const& t)
  {
    strncpy(data, reinterpret_cast<char const*>(&t), sizeof(T));

    return sizeof(T);
  }

  // Writes the given type into the data bytewise
  // Returns the number of bytes written
  std::size_t write(char* data, char const* string, std::size_t const length)
  {
    data += write(data, length);
    strncpy(data, string, length);
    return length + sizeof(std::size_t);
  }

  // Reads the given type from stream and returns it
  template <typename T> T read(std::istream& stream)
  {
    T t;
    stream.read(reinterpret_cast<char*>(&t), sizeof(T));
    return t;
  }

  template <> std::string read(std::istream& stream)
  {
    std::size_t const stringLength = read<std::size_t>(stream);

    std::string string;
    if(stringLength > 0)
    {
      string.resize(stringLength);
      stream.read(string.data(), stringLength);
    }

    return string;
  }

  // Function which tells the caller how many bytes are to be written for the given argument
  template <typename T> constexpr std::size_t bytesToWrite(T const& value) { return sizeof(value); }

  constexpr std::size_t bytesToWrite(char const* string, std::size_t const length)
  {
    return bytesToWrite(length) + length;
  }
}
}