#pragma once

#include "cppfulltext/indexstorage.h"

#include "gen/export_fulltext.h"
#include <memory>

namespace CppFulltext
{
class IndexStorage;

// Returns an instance of filestorage pointing to 'directoryPath' location.
// If the folder does not exist or it is not a folder, returns nullptr!
FULLTEXT_EXPORT std::unique_ptr<IndexStorage> createFileStorage(char const* directoryPath,
                                                                bool const createNew = false);
}