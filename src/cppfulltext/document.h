#pragma once

#include <string>
#include <unordered_map>

#include "gen/export_fulltext.h"

namespace CppFulltext
{
class Document
{
private:
  std::unordered_map<std::string, std::string> fields;

public:
  Document(Document const&);

  FULLTEXT_EXPORT explicit Document();

  // Adds a field to the contents of this document. overrides previously set fields with the same
  // name
  FULLTEXT_EXPORT void add(std::string const& field, std::string const& content);

  // Removes the field with the given name
  FULLTEXT_EXPORT void remove(std::string const& field);

  // Gets a specific field of the document. Returns empty string if this field does not exist
  FULLTEXT_EXPORT std::string get(std::string const& field) const;

  // Returns the contents of this document for iteration
  FULLTEXT_EXPORT std::unordered_map<std::string, std::string> const& getFields() const;

  FULLTEXT_EXPORT std::unordered_map<std::string, std::string>& getFields();
};
}