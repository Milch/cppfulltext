#pragma once

#include "gen/export_fulltext.h"
#include <memory>
#include <set>
#include <string>
#include <unordered_map>
#include <vector>

#include "indexserializer.h"

namespace CppFulltext
{
class Document;
class IndexStorage;
class Query;

class IndexReader
{
private:
  std::unique_ptr<IndexStorage> indexStorage;
  bool isValidInternal;

  Serializer::ContentMap contents;
  std::size_t generation;
  std::unordered_map<unsigned int, std::unique_ptr<Document>> documents;
  std::set<unsigned int> deletedDocuments;

public:
  bool search(std::string const& field,
              std::string const& term,
              std::size_t const callerGeneration,
              std::vector<unsigned int>& output) const;

  FULLTEXT_EXPORT explicit IndexReader(std::unique_ptr<IndexStorage>&& indexStorage);
  FULLTEXT_EXPORT ~IndexReader();
  FULLTEXT_EXPORT bool isValid() const;
  FULLTEXT_EXPORT std::unique_ptr<Query> query() const;
  FULLTEXT_EXPORT std::unique_ptr<Document> get(unsigned int docId);
  FULLTEXT_EXPORT void update();
};
}