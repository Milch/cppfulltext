#pragma once

#include <string>
#include <utility>
#include <vector>

#include "gen/export_fulltext.h"

namespace CppFulltext
{
class IndexReader;

class Query
{
private:
  std::vector<std::pair<std::string, std::string>> searchTerms;
  IndexReader const& reader;
  std::size_t const generation;

public:
  // Note: Not exported constructor, outside cannot instantiate!
  Query(IndexReader const& reader, std::size_t const generation);

  FULLTEXT_EXPORT Query* contains(std::string const& field, std::string const& content);
  FULLTEXT_EXPORT std::vector<unsigned int> perform();
};
}