#include "filestorage.h"
#include "indexstorage.h"

#include <cstring>
#include <fstream>
#include <memory>

#include <experimental/filesystem>
namespace fs = std::experimental::filesystem;

namespace CppFulltext
{
class FileStorage : public IndexStorage
{
private:
  char const* directoryPath;

public:
  explicit FileStorage(char const* directoryPath) : directoryPath(directoryPath) {}

  ~FileStorage() = default;

  std::vector<std::string> getFileList()
  {
    std::vector<std::string> retVal;
    for(auto const& entry : fs::directory_iterator(directoryPath))
      retVal.emplace_back(entry.path().filename().string());

    return retVal;
  }

  std::unique_ptr<std::istream> read(char const* fileName)
  {
    std::string fullPath;
    fullPath += directoryPath;
    fullPath += "//";
    fullPath += fileName;

    auto ptr =
      std::make_unique<std::fstream>(fullPath.c_str(), std::ios::binary | std::fstream::in);

    if(!ptr->good())
    {
      return std::unique_ptr<std::istream>();
    }

    return std::move(ptr);
  }

  bool write(char const* fileName, char const* content, std::size_t const length)
  {
    std::string fullPath;
    fullPath += directoryPath;
    fullPath += "//";
    fullPath += fileName;

    std::fstream file;
    file.open(fullPath.c_str(), std::ios::binary | std::fstream::out);
    file.write(content, length);
    file.close();

    return true;
  }

  void deleteFile(char const* fileName)
  {
    std::string fullPath;
    fullPath += directoryPath;
    fullPath += "//";
    fullPath += fileName;
    std::remove(fullPath.c_str());
  }
};

std::unique_ptr<IndexStorage> createFileStorage(char const* directoryPath, bool const createNew)
{
  if(createNew)
  {
    if(fs::exists(directoryPath))
    {
      if(!fs::is_empty(directoryPath))
      {
        try
        {
          if(!fs::remove_all(directoryPath))
          {
            return std::unique_ptr<IndexStorage>();
          }
        }
        catch(...)
        {
          // bug in mingw32-w64, removing of dir throws exception even if it is removed
          if(fs::exists(directoryPath) && !fs::is_empty(directoryPath))
          {
            return std::unique_ptr<IndexStorage>();
          }
        }
      }
    }
  }

  fs::create_directory(directoryPath);
  return std::make_unique<FileStorage>(directoryPath);
}
}