#pragma once

#include <iostream>
#include <memory>
#include <vector>

namespace CppFulltext
{
class IndexStorage
{
public:
  virtual ~IndexStorage() = default;

  // Gets the list of files of this point in time.
  virtual std::vector<std::string> getFileList() = 0;

  // Opens a file for reading. If file does not exist or some other error occured,
  // the stream.good() function is expected to return false!
  virtual std::unique_ptr<std::istream> read(char const* fileName) = 0;

  // Open a file and writes content into it. Creates file if it does not exist, otherwise truncates
  virtual bool write(char const* fileName, char const* content, std::size_t const length) = 0;

  // Delete a file, if it does not exist, nothing happens
  virtual void deleteFile(char const* fileName) = 0;
};
}