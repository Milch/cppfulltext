#include "indexreader.h"
#include "document.h"
#include "indexstorage.h"
#include "query.h"
#include "utils.h"

namespace CppFulltext
{

IndexReader::IndexReader(std::unique_ptr<IndexStorage>&& indexStorage)
  : indexStorage(std::move(indexStorage)), generation(0)
{
  isValidInternal = true;
  update();
}

IndexReader::~IndexReader() {}

bool IndexReader::isValid() const { return isValidInternal; }
std::unique_ptr<Query> IndexReader::query() const
{
  return std::make_unique<Query>(*this, generation);
}
std::unique_ptr<Document> IndexReader::get(unsigned int docId)
{
  auto const it = documents.find(docId);
  if(it == documents.end()) return std::unique_ptr<Document>();

  return std::make_unique<Document>(*(it->second));
}

bool IndexReader::search(std::string const& field,
                         std::string const& term,
                         std::size_t const callerGeneration,
                         std::vector<unsigned int>& output) const
{
  if(generation != callerGeneration)
  {
    std::cerr << "Query perform with old generation!" << std::endl;
    return false;
  }

  auto it = contents.find(field);
  if(it == contents.end())
  {
    return true;
  }

  auto termIt = it->second.find(term);
  if(termIt == it->second.end())
  {
    return true;
  }

  output.clear();
  output.reserve(termIt->second.size());
  for(unsigned int const docId : termIt->second)
  {
    if(deletedDocuments.find(docId) != deletedDocuments.end()) continue;
    output.push_back(docId);
  }

  return true;
}

void IndexReader::update()
{
  auto pair = Serializer::getLatestIndex(*indexStorage);
  if(pair.second == 0)
  {
    std::cerr << "No index was found!" << std::endl;
    isValidInternal = false;
    return;
  }

  if(!Serializer::deserialize(*indexStorage.get(), pair.first.c_str(), contents, documents,
                              deletedDocuments))
  {
    std::cerr << "Failed deserializing index!" << std::endl;
    return;
  }
  generation++;
}
}