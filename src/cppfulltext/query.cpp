#include "query.h"
#include "indexreader.h"

namespace CppFulltext
{
Query::Query(IndexReader const& reader, std::size_t const generation)
  : reader(reader), generation(generation)
{
}

Query* Query::contains(std::string const& field, std::string const& content)
{
  searchTerms.push_back(std::make_pair(field, content));
  return this;
}
std::vector<unsigned int> Query::perform()
{
  std::vector<unsigned int> finalDocuments;
  std::vector<unsigned int> curDocuments;
  for(auto const & [ field, term ] : searchTerms)
  {
    if(!reader.search(field, term, generation, curDocuments))
    {
      return std::vector<unsigned int>();
    }

    if(finalDocuments.size() == 0)
    {
      finalDocuments.swap(curDocuments);
      continue;
    }

    // Very bad merging algorithm
    std::vector<unsigned int> tmp;
    tmp.reserve(finalDocuments.size());
    for(auto const& newDocId : curDocuments)
    {
      for(auto const& otherDocId : finalDocuments)
      {
        if(newDocId == otherDocId)
        {
          tmp.push_back(newDocId);
          break;
        }
      }
    }

    finalDocuments.swap(tmp);
  }

  return finalDocuments;
}
}