# README #

Fulltext-search implemented with pure C++17

For creating a buildable IDE with those sources open a cmd tool in your target folder, and use:
cmake -G [IDEName] [PathToRepo]

### Build Status ###
[![CircleCI](https://circleci.com/bb/Milch/cppfulltext.svg?style=svg)](https://circleci.com/bb/Milch/cppfulltext)




## API usage ##


#*Document*
The class which contains the data and gets indexed

	ctor: Document();

	void add(std::string const& field, std::string const& content);
		Adds a field to the contents of this document. overrides previously set fields with the same name
	void remove(std::string const& field);
		Removes the field with the given name
	std::string get(std::string const& field) const;
		Gets a specific field of the document. Returns empty string if this field does not exist
	std::unordered_map<std::string, std::string> const& getFields() const;
		Returns the contents of this document for iteration


#*MemoryStorage* : IndexStorage
Used for creating a small, non-file based index

	ctor: createMemoryStorage(MemoryStorageBackend)
		Returns a shared pointer to a new MemoryStorage
		The MemoryStorageBackend struct is needed in order to share a single memory storage with multiple readers/single write


#*FileStorage* : IndexStorage
Used for creating larger, file based index

	ctor: createFileStorage(char const* directoryPath)
		Returns a shared pointer to a new FileStorage, nullptr if it does not exist
		The index files will be created in the given directoryPath


#*IndexWriter*
Used for building and writing the index to an IndexStorage.
Only one instance of IndexWriter should exist at a time.

	ctor: IndexWriter(std::unique_ptr<IndexStorage>&& indexStorage);

	bool isValid() const; 
		Represents the validity of the object, if it is invalid, it will do nothing
	void addDocuments(Document* docs, std::size_t const docNum);
		Adds the documents from a given document list (pointer to first document) to the index
	bool deleteDocument(unsigned int documentId);
		Deletes the document with the given id
		Returns false if unsuccessfull
	void flush();
		flushes the added and deleted documents to the indexStorage
	void close();
		flushes and invalidates the object


#*IndexReader*
Used for getting query objects and documents

	ctor: IndexReader(std::unique_ptr<IndexStorage>&& indexStorage);

	bool isValid() const;
		Represents the validity of the object, if it is invalid, it will do nothing
	std::unique_ptr<Query> query() const;
		Returns a new query object, which can be used to query the index
	std::shared_ptr<Document const> get(unsigned int docId);
		Returns a pointer to an existing document in the index (nullptr on not found)
	void update();
		gets latest changes from IndexStorage


#*Query*
Used for performing queries on the index.
Can only be created by IndexReader

	ctor: none

	Query* contains(std::string const& field, std::string const& content);
		Adds a constraint to the Query
	std::vector<unsigned int> perform();
		Executes the query, and returns a list of document ids which meet the search criteria


## How to contribute ##

CppFulltext is worked on by a project team composed of students.
If you want to contribute, you can fork the repository and continue the work after we stopped the project.